#include <stdio.h>

int main() {
    int array[] = {10, 20, 30, 40, 50, 60, 70};
    int array_len = sizeof(array) / sizeof(array[0]);

    for (int i = 0; i < array_len; ++i) {
        printf("%d\t", array[i]);
    }
    printf("\n");

    int matrix_a[2][2] = {
            {1, 2},
            {3, 1},
    };
    int matrix_b[2][2] = {
            {1, 1},
            {0, 2},
    };

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int element = 0;
            for (int k = 0; k < 2; ++k) {
                element += matrix_a[i][k] * matrix_b[k][j];
            }
            printf("%d\t", element);
        }
        printf("\n");
    }

    return 0;
}