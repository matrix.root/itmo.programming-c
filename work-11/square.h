#ifndef PROGRAMMING_LABS_SQUARE_H

#define PROGRAMMING_LABS_SQUARE_H

struct Point {
    int x, y;
};

struct Square {
    struct Point begin, end;
};

struct Square get_square_from_console();

double get_area(struct Square);

int get_perimeter(struct Square);

#endif //PROGRAMMING_LABS_SQUARE_H
