#include <stdio.h>
#include "square.h"

int main() {
    struct Square square = get_square_from_console();
    printf("Area: %.2lf\n", get_area(square));
    printf("Perimeter: %d", get_perimeter(square));
    return 0;
}

