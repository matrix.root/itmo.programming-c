#include <time.h>
#include <stdio.h>


int main(int argc, char *argv[]) {
    FILE *output_file = fopen(argv[1], "w");

    time_t t = time(NULL);
    struct tm *next_time = localtime(&t);
    for (int i = 0; i < 10; ++i) {
        char buffer[256];
        strftime(buffer, sizeof(buffer), "%d.%m.%y", next_time);
        fprintf(output_file, "%s\n", buffer);

        next_time->tm_mday += 1;
        t = mktime(next_time);
        next_time = localtime(&t);
    }

    return 0;
}
