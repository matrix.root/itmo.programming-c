#include <stdio.h>
#include <math.h>

int main() {
    double a;
    scanf("%lf", &a);

    double z1 = (1 - 2 * pow(sin(a), 2)) / (1 + sin(a * 2));
    double tangent = tan(a);
    double z2 = (1 - tangent) / (1 + tangent);

    printf("%lf\n%lf\n", z1, z2);
    return 0;
}