#include <stdio.h>
#include <MacTypes.h>

enum week {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
};


struct Square {
    int x1, y1, x2, y2;
};

union Keyboard {
    struct {
        unsigned is_num_enabled: 1;
        unsigned is_caps_enabled: 1;
        unsigned is_scroll_enabled: 1;
    } flags;
    unsigned state;
};

int main() {
    enum week task_1 = Monday;
    printf("%d\n", task_1);

    struct Square square;
    square.x1 = 1;
    square.y1 = 3;
    square.x2 = -2;
    square.y2 = 5;
    int task_2 = abs(square.x1 - square.x2) * abs(square.y1 - square.y2);
    printf("%d\n", task_2);

    union Keyboard task_3;
    scanf("%x", &task_3.state);
    printf("%d %d %d\n", task_3.flags.is_num_enabled, task_3.flags.is_caps_enabled, task_3.flags.is_scroll_enabled);

    return 0;
}