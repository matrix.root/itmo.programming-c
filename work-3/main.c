#include <stdio.h>

int main() {
    int number;
    scanf("%d", &number);
    printf("%X\n", number);
    printf("%o %o\n", number, number << 3);
    printf("%o %o\n", number, ~number);
    int second_number;
    scanf("%o", &second_number);
    printf("%o\n", number & second_number);
    return 0;
}