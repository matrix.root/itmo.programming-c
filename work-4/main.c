#include <stdio.h>

int main() {
    int number;
    scanf("%d", &number);
    int in_range = (11 <= number) && (number <= 12);
    printf("%d\n", in_range);

    int bit_check_umber;
    scanf("%d", &bit_check_umber);
    printf("%d\n", bit_check_umber >> 10 & 1);

    return 0;
}