#include <stdio.h>
#include <memory.h>
#include <ctype.h>


void task_1_symbols();

void task_2_phone();

int main() {
    task_1_symbols();
    task_2_phone();
    return 0;
}

void task_1_symbols() {
    const int MAX_LEN = 256;
    char str[MAX_LEN];
    fgets(str, MAX_LEN, stdin);
    int digit_cnt = 0, upper_cnt = 0, lower_cnt = 0;
    for (int i = 0; i < strlen(str); ++i) {
        char c = str[i];
        if (isdigit(c)) {
            ++digit_cnt;
        } else if (islower(c)) {
            ++lower_cnt;
        } else if (isupper(c)) {
            ++upper_cnt;
        }
    }
    printf("String have %d digits, %d lowers, %d uppers\n", digit_cnt, lower_cnt, upper_cnt);
}

void task_2_phone() {
    const int free_minutes = 499;
    int talking_duration, subscription_payment, extra_minute_price;
    printf("Enter talking duration, subscription payment abd extra minute price: ");
    scanf("%d %d %d", &talking_duration, &subscription_payment, &extra_minute_price);
    int extra_minutes = talking_duration - free_minutes;
    if (extra_minutes > 0) {
        subscription_payment += extra_minutes * extra_minute_price;
    }
    printf("Total payment: %d\n", subscription_payment);
}