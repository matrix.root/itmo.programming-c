#include <stdio.h>
#include <memory.h>
#include <ctype.h>
#include <stdlib.h>

void task_1_gdc_and_lcm();

void task_2_points_distances();


int main() {
    task_1_gdc_and_lcm();
    task_2_points_distances();
    return 0;
}


int gcd(int a, int b) {
    return b ? gcd(b, a % b) : a;
}

int lcm(int a, int b) {
    return a / gcd(a, b) * b;
}


void task_1_gdc_and_lcm() {
    int a, b;
    scanf("%d %d", &a, &b);
    printf("GCD: %d; LCM: %d\n", gcd(a, b), lcm(a, b));
}

struct Point {
    int x, y;
};

double get_distance(struct Point point, struct Point point1);


void task_2_points_distances() {
    int n;
    scanf("%d", &n);
    struct Point *points = (struct Point *) malloc(n * sizeof(struct Point));
    for (int i = 0; i < n; ++i) {
        struct Point *p = &points[i];
        scanf("%d %d", &p->x, &p->y);
    }

    int neighbors_cnt = n - 1;
    double *distances = (double *) malloc(n * neighbors_cnt * sizeof(double));
    for (int from = 0; from < n; ++from) {
        printf("From %d |\t", from);
        for (int to_index = 0; to_index < neighbors_cnt; ++to_index) {
            int to = to_index < from ? to_index : to_index + 1;
            int d_index = from * neighbors_cnt + to_index;
            distances[d_index] = get_distance(points[from], points[to]);
            printf("To %d: %.2lf |\t", to, distances[d_index]);
        }
        printf("\n");
    }

}

double get_distance(struct Point a, struct Point b) {
    return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
}
