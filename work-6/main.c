#include <stdio.h>

int main() {
    char array[4];
    array[0] = 'a';
    array[1] = 'b';
    array[2] = 'c';
    array[3] = 'd';
    for (int i = 0; i < 4; ++i) {
        printf("%c\t", *(array + i));
    }
    printf("\n");

    char *array_d;
    array_d = (char *) malloc(4 * sizeof(char));
    array_d[0] = 'a';
    array_d[1] = 'b';
    array_d[2] = 'c';
    array_d[3] = 'd';
    for (int i = 0; i < 4; ++i) {
        printf("%c\t", array_d[i]);
    }
    free(array_d);

    return 0;
}